variable "hcloud_token"  {
    default = ""
}

variable "cluster_name"  {
    default = ""
}

variable "servers" {
    type = map
}

# Optional configuration

variable "hcloud_location" {
  default = "nbg1"
}
variable "private_ip_range" {
  default = "10.0.0.0/16"
}
variable "ssh_public_key_name" {
  default = "strong"
}
variable "private_network_name" {
  default = "default"
}
variable "private_network_zone" {
  default = "eu-central"
}
variable "floating_ip_name" {
  default = "default"
}
variable "install_ansible_dependencies" {
  default = true
}
variable "ansible_dependencies_install_command" {
  default = "sudo yum install -y python36"
}
variable "run_ansible_playbook" {
  default = true
}
variable "post_ansible_ssh_user" {
  default = "deploy"
}
variable "remote_user" {
  default = "root"
}
# Path to your public key, to log in to instances
variable "public_key" {
  default = "~/.ssh/id_rsa.pub"
}

# Path to your private key that matches your public from ^^
variable "private_key" {
  default = "~/.ssh/id_rsa"
}
