#!/usr/bin/env bash

# Short form: set -u
set -o nounset
# Short form: set -e
set -o errexit

# Print a helpful message if a pipeline with non-zero exit code causes the
# script to exit as described above.
#trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR

# Allow the above trap be inherited by all functions in the script.
#
# Short form: set -E
#set -o errtrace

# Return value of a pipeline is the value of the last (rightmost) command to
# exit with a non-zero status, or zero if all commands in the pipeline exit
# successfully.
#set -o pipefail

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
IFS=$'\n\t'

###############################################################################
# Program Functions
###############################################################################

_verify_ansible() {
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py --no-wheel --user
export PATH="/home/terraform/.local/bin:$PATH"
pip install --user ansible
pip install --user hcloud
if [ -x "$(command -v /home/terraform/.local/bin/ansible-galaxy)" ]; then
	# install role
  #ansible-galaxy install git+https://github.com/elastic/ansible-elastic-cloud-enterprise.git
  ansible-galaxy install -r k3s_ansible/requirements.yml
else
  echo "ERROR: Ansible isn't installed on this machine, aborting installation"
  exit 1
fi
}

_write_ansible_playbook() {
cat << PLAYBOOK > ./provision_not_used.yml
---
- name: Node provisioning
  hosts: all
  remote_user: root
  become: yes
  become_method: sudo
  roles:
    - role: prereq
      tags: prereq
    - role: ansible-bootstrap-role
      tags: bootstrap
    - role: ansible-fail2ban-role
      tags: fail2ban
    - role: ansible-hcloud-floating-ip-role
      tags: floating_ip
PLAYBOOK
}

_write_ansible_hosts() {
cat << HOSTS_FILE > ./private_key.pem
${key}
HOSTS_FILE
chmod 600 ./private_key.pem
}

_run_ansible() {
  export ANSIBLE_HOST_KEY_CHECKING=False
  ansible-playbook --key-file ./private_key.pem -i k3s_ansible/hcloud.yml k3s_ansible/provision.yml
}

###############################################################################
# Main
###############################################################################

# _main()
#
# Usage:
#   _main [<options>] [<arguments>]
#
# Description:
#   Entry point for the program, handling basic option parsing and dispatching.
_main() {
    _verify_ansible
    _write_ansible_playbook
    _write_ansible_hosts
    sleep 30
    _run_ansible
}

# Call `_main` after everything has been defined.
_main "$@"
