servers = {
  1 = {
    name               = "test-master1"
    private_ip_address = "10.0.0.2"
    server_type        = "cx11"
    image              = "centos-7"
    location           = "nbg1"
    backups            = false
  },
  2 = {
    name               = "test-master2"
    private_ip_address = "10.0.0.3"
    server_type        = "cx11"
    image              = "centos-7"
    location           = "nbg1"
    backups            = false
  },
  3 = {
    name               = "test-master3"
    private_ip_address = "10.0.0.4"
    server_type        = "cx11"
    image              = "centos-7"
    location           = "nbg1"
    backups            = false
  },
}
