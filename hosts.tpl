     ${nome}:
       ansible_host: ${ip}
       k3s_control_node: false
       k3s_node_external_address: ${ip}
       k3s_flannel_interface: ens10
