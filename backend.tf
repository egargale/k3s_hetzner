terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "glegal"

    workspaces {
      name = "hetzner"
    }
  }
}
