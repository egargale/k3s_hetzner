provider "hcloud" {
  token = var.hcloud_token
}

resource "hcloud_network" "default" {
  name     = var.private_network_name
  ip_range = var.private_ip_range
}

resource "hcloud_network_subnet" "default" {
  network_id   = "${hcloud_network.default.id}"
  type         = "server"
  network_zone = var.private_network_zone
  ip_range     = var.private_ip_range
}

resource "hcloud_floating_ip" "default" {
  type          = "ipv4"
  home_location = "${var.hcloud_location}"
  name          = var.floating_ip_name
}

resource "hcloud_server" "server" {
  for_each = var.servers

  name        = each.value.name
  image       = each.value.image
  server_type = each.value.server_type
  location    = each.value.location
  backups     = each.value.backups
  ssh_keys    = ["strong"]
}

resource "hcloud_server_network" "server_network" {
  for_each = var.servers

  network_id = hcloud_network.default.id
  server_id  = hcloud_server.server[each.key].id
  ip         = each.value.private_ip_address
}

resource "hcloud_floating_ip_assignment" "default" {
  server_id      = "${hcloud_server.server["1"].id}"
  floating_ip_id = "${hcloud_floating_ip.default.id}"
}

#module "ansible_provisioner" {
#  source    = "github.com/cloudposse/tf_ansible"
#
#  arguments = ["--user=root"]
#  envs      = ["host=${hcloud_server.server["1"].ipv4_address}"]
#  playbook  = "ansible/provision.yml"
#  dry_run   = true
#}

data "template_file" "hosts" {
  template = "${file("${path.module}/hosts.tpl")}"
  depends_on = [hcloud_server.server]
  count = "${length(hcloud_server.server)}"
  vars = {
    nome = "${values(hcloud_server.server)[count.index].name}"
    ip = "${values(hcloud_server.server)[count.index].ipv4_address}"
   }
}
resource "local_file" "save_inventory" {
  content = templatefile("${path.module}/inventory.tpl",
  {floating_ip  = "${hcloud_floating_ip.default.ip_address}",
  value = "${join("", data.template_file.hosts.*.rendered)}" })
  filename = "k3s_inventory.yaml"
}

data "template_file" "ansible-install" {
  template = file("ansible-install.sh")
  depends_on = [hcloud_server.server]
  vars = {
    # Keys to server
    key = var.private_key
    # User to login
    user = var.remote_user
  }
}

# Initiate installation through ansible playbook
resource "null_resource" "run-ansible" {
  provisioner "local-exec" {
    command = data.template_file.ansible-install.rendered
  }
}
